﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment
{
    //enum equipmentType { Weapon, Frame, Accessory}
    //equipmentType etype;
    [SerializeField] public string equipmentName = "Nothing?";
    public int might = 1;
    public int hit = 70;
    public int crit = 5;

    public List<BattleAction> equipmentActions;

    public Equipment (string eName, int eMight, int eHit, int eCrit)
    {
        equipmentName = eName;
        might = eMight;
        hit = eHit;
        crit = eCrit;
    }
}
