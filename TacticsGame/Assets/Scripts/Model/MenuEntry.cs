﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuEntry : MonoBehaviour
{
    [System.Flags] //TODO, What does this do?
    public enum States
    {
        None = 0,
        Selected = 1 << 0 //TODO, figure out what this means.
    }

    public Text label;
    //public Image button;
    States state;

    public string Title
    {
        get { return label.text; }
        set { label.text = value; }
    }


    public States State
    {
        get { return state; }
        set
        {
            if (state == value)
                return;
            state = value;

            Image img = gameObject.GetComponent<Image>();

            if (IsSelected)
            {
                img.color = new Color32(255, 103, 7, 255);
                label.color = new Color32(255, 239, 173, 255);
            }
            else
            {
                img.color = new Color32(255, 239, 173, 255);
                label.color = Color.gray;
            }

        }
    }

    public bool IsSelected
    {
        get { return (State & States.Selected) != States.None; }
        set
        {
            if (value)
                State |= States.Selected; //TODO, What does this do?
            else
                State &= ~States.Selected; //TODO, What does this do?
        }
    }

    public void Reset()
    {
        State = States.None;
    }
}
