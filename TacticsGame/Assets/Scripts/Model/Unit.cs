﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Unit : MonoBehaviour
{

    public BattleTile tile { get; set; }    //Unit's tile
    //public bool canAct;                     //Has the unit acted yet this turn?

    public int maxHealth = 10;
    public int currentHealth = 10;
    public int strength = 1;
    public int magic = 1;
    public int skill = 1;
    public int speed = 1;
    public int luck = 1;
    public int defense = 1;
    public int resistance = 1;

    [SerializeField] public Equipment equipment;

    private bool canAct;
    public bool CanAct
    {
        get { return canAct; }
        set
        {
            if (canAct == value)
                return;
            canAct = value;

            SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
            if(canAct)
            {
                renderer.color = new Color(1f, 1f, 1f);
            }
            else
            {
                renderer.color = new Color(.25f, .25f, .25f);
            }
        }
    }

    public void Place (BattleTile target)
    {
        //Unlink current tile if necessary.
        if(tile != null && tile.content == gameObject)
        {
            tile.content = null;
        }

        //Link to new tile.
        tile = target;
        if (target != null)
        {
            target.content = gameObject;
        }
    }

    //#TODO give this a better name.
    public void Match()
    {
        transform.localPosition = tile.parentTileMap.GetCellCenterLocal(tile.localPosition.ToVector3Int());
    }

    public int calculatePower()
    {
        return strength + equipment.might;
    }
}
