﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BattleAction
{
    public string actionName = "Do Nothing?";
    public string actionDescription = "...";
    public int actionDamage = 0;
    List<int> actionRange = null;

    public BattleAction(string name, int damage, List<int> range)
    {
        actionName = name;
        actionDamage = damage;
        actionRange = range;
    }
}
