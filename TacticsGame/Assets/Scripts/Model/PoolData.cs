﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//http://theliquidfire.com/2015/07/06/object-pooling/
public class PoolData
{
    public GameObject prefab;
    public int maxCount;
    public Queue<Poolable> pool;
}
