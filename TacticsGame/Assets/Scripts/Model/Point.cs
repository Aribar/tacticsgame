﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Point
{
    public int x;
    public int y;

   public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Point(Vector3Int v)
    {
        this.x = v.x;
        this.y = v.y;
    }

    //Overriding operators to add points together... Not sure if this is worthwhile?
    public static Point operator +(Point a, Point b)
    {
        return new Point(a.x + b.x, a.y + b.y);
    }
    public static Point operator -(Point a, Point b)
    {
        return new Point(a.x - b.x, a.y - b.y);
    }
    public static bool operator ==(Point a, Point b)
    {
        return a.x == b.x && a.y == b.y;
    }
    public static bool operator !=(Point a, Point b)
    {
        return !(a == b);
    }
    public override bool Equals (object obj)
    {
        if (obj is Point)
        {
            Point p = (Point)obj;
            return x == p.x && y == p.y;
        }
        return false;
    }
    public bool Equals (Point p)
    {
        return x == p.x && y == p.y;
    }
    public override int GetHashCode()
    {
        return x ^ y;
    }

    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }

    public Vector3Int ToVector3Int()
    {
        return new Vector3Int(x, y, 0);
    }
}
