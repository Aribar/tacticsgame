﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BattleBoard : MonoBehaviour
{
    public Tilemap battleMap;
    public Dictionary<Point, BattleTile> battleTiles = new Dictionary<Point, BattleTile>();

    Color selectedTileColor = new Color(0, 1, 1, 1);
    Color defaultTileColor = new Color(1, 1, 1, 1);

    readonly Point[] dirs = new Point[4]
    {
        new Point(0, 1),
        new Point(0, -1),
        new Point(1, 0),
        new Point(-1, 0)
    };

    /// <summary>
    /// Clears the battleMap of existing tiles and then arbitrarily sets them.
    /// </summary>disk
    public void InitializeTileMap(int mapWidth, int mapHeight, TileBase tile1, TileBase tile2)
    {
        battleMap.ClearAllTiles();
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                Vector3Int pos = new Vector3Int(x, y, 0);
                if ((x + y) % 2 == 1)
                    battleMap.SetTile(pos, tile1);
                else
                    battleMap.SetTile(pos, tile2);

                battleMap.SetTileFlags(pos, TileFlags.None);
            }

        }
    }

    public void InitializeBattleTileData()
    {
        //battleTiles = new Dictionary<Point, BattleTile>();
        foreach (Vector3Int pos in battleMap.cellBounds.allPositionsWithin)
        {
            if (battleMap.HasTile(pos))
            {
                BattleTile battleTile = new BattleTile
                {
                    localPosition = new Point(pos.x, pos.y),//new Vector3Int(pos.x, pos.y, pos.z),
                    parentTileMap = battleMap,
                    movementCost = 1
                };
                battleTiles.Add(battleTile.localPosition, battleTile);
            }
        }
    }

    #region Movement Stuff
    public List<BattleTile> Search (BattleTile start, Func<BattleTile, BattleTile, bool> addTile)
    {
        List<BattleTile> retValue = new List<BattleTile>();
        retValue.Add(start);

        ClearSearch();
        Queue<BattleTile> checkNext = new Queue<BattleTile>();
        Queue<BattleTile> checkNow = new Queue<BattleTile>();
        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            //Checking all the tiles surrounding a tile.
            BattleTile t = checkNow.Dequeue();
            for(int i = 0; i < 4; ++i)
            {
                //Get the next tile and if it's a shorter distance then we go onward.
                BattleTile next = GetTile(t.localPosition + dirs[i]);
                if(next == null || next.distance <= t.distance + 1)
                {
                    continue;
                }

                if(addTile(t, next))
                {
                    next.distance = t.distance + 1; //Could here be where I'd implement a movement cost?
                    next.previousBattleTile = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            //Did we get everything?
            if(checkNow.Count == 0)
            {
                SwapReference(ref checkNow, ref checkNext);
            }
        }


        return retValue;
    }

    public void SelectTiles (List<BattleTile> tiles)
    {
        for(int i = tiles.Count -1; i >= 0; --i)
        {
            tiles[i].parentTileMap.SetColor(tiles[i].localPosition.ToVector3Int(), selectedTileColor);
        }
    }

    public void DeselectTiles(List<BattleTile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            tiles[i].parentTileMap.SetColor(tiles[i].localPosition.ToVector3Int(), defaultTileColor);
        }
    }

    void SwapReference(ref Queue<BattleTile> a, ref Queue<BattleTile> b)
    {
        Queue<BattleTile> temp = a;
        a = b;
        b = temp;
    }

    public BattleTile GetTile(Point p)
    {
        return battleTiles.ContainsKey(p) ? battleTiles[p] : null;
    }

    void ClearSearch()
    {
        foreach(BattleTile t in battleTiles.Values)
        {
            t.previousBattleTile = null;
            t.distance = int.MaxValue;
        }
    }
    #endregion

}
