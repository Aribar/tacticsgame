﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetedActionUnitSummaryPanel : MonoBehaviour
{

    public Text playerUnitNameLabel;
    public Text playerUnitEquipmentLabel;
    public Text targetUnitNameLabel;
    public Text targetUnitEquipmentLabel;
    public Text playerHP;
    public Text playerPower;
    public Text playerHit;
    public Text playerCrit;
    public Text targetHP;
    public Text targetPower;
    public Text targetHit;
    public Text targetCrit;

    public string GetText(Text text)
    {
        return text.text;
    }

    public void SetText(Text text, string value)
    {
        text.text = value;
    }
}
