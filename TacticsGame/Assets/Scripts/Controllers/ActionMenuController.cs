﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ActionMenuController : MonoBehaviour
{
    public GameObject menuEntryPrefab;
    //public bool visible { get; protected set; }
    public GameObject canvas; //The canvas that the menu will go on.
    public GameObject panel; //The panel that all the entries will go in.

    List<MenuEntry> currentEntries = new List<MenuEntry>();
    public int selection { get; private set; }

    const string ShowKey = "Show";
    const string HideKey = "Hide";
    const string EntryPoolKey = "ActionMenu.Entry";
    const int MenuCount = 2;

    void Awake()
    {
        PoolController.AddEntry(EntryPoolKey, menuEntryPrefab, MenuCount, int.MaxValue); //... Is that last bit possibly right?
    }

    void Start()
    {
        //panel.SetPosition(HideKey, false); //What is this?
        canvas.SetActive(false);
    }

    public MenuEntry Dequeue()
    {
        GameObject entryGO = PoolController.Dequeue(EntryPoolKey).gameObject;
        MenuEntry entry = entryGO.GetComponent<MenuEntry>();
        entryGO.transform.SetParent(panel.transform, false);
        entryGO.transform.localScale = Vector3.one; //Do I need to set the local scale?
        entryGO.gameObject.SetActive(true);
        entry.Reset();
        return entry;
    }

    void Enqueue(MenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        PoolController.Enqueue(p);
    }

    void Clear()
    {
        for (int i = currentEntries.Count - 1; i >= 0; --i)
            Enqueue(currentEntries[i]);
        currentEntries.Clear();
    }

    /// <summary>
    /// Deselect the previous entry and select the new entry.
    /// </summary>
    /// <param name="value">New entry to be selected.</param>
    /// <returns></returns>
    bool SetSelection(int value)
    {
        if (selection >= 0 && selection < currentEntries.Count)
            currentEntries[selection].IsSelected = false;
        selection = value;
        if (selection >= 0 & selection < currentEntries.Count)
            currentEntries[selection].IsSelected = true;
        return true;
    }

    /// <summary>
    /// Move to an adjacent menu entry.
    /// </summary>
    /// <param name="next">If true, go down. If false, go up.</param>
    public void ChangeSelection(bool next)
    {
        int index;
        if (next)
            index = (selection + 1) % currentEntries.Count;
        else
            index = (selection - 1 + currentEntries.Count) % currentEntries.Count;
        SetSelection(index);
    }

    /// <summary>
    /// Load and display the menu.
    /// </summary>
    /// <param name="title"></param>
    /// <param name="options">Menu options to be displayed.</param>
    public void ShowMenu(string title, List<string> options)
    {
        canvas.SetActive(true);
        Clear();
        ////Don't think I need title...

        for (int i = 0; i < options.Count; ++i)
        {
            MenuEntry entry = Dequeue();
            entry.Title = options[i];
            currentEntries.Add(entry);
        }
        SetSelection(0);
    }

    /// <summary>
    /// Hide menu and deactivate canvas.
    /// </summary>
    public void HideMenu()
    {
        Clear();
        canvas.SetActive(false);
    }
}
