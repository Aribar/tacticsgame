﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetedActionMenuController : MonoBehaviour
{
    public GameObject summaryPanelPrefab;
    public GameObject summaryPanelReference;
    public GameObject canvas;


    const string EntryPoolKey = "TargetedActionMenu.SummaryPanel";
    const int MenuCount = 1;

    private void Awake()
    {
        PoolController.AddEntry(EntryPoolKey, summaryPanelPrefab, MenuCount, int.MaxValue);
    }

    // Start is called before the first frame update
    void Start()
    {
        canvas.SetActive(false);
    }

    public GameObject Dequeue()
    {
        summaryPanelReference = PoolController.Dequeue(EntryPoolKey).gameObject;
        summaryPanelReference.transform.SetParent(canvas.transform, false);
        summaryPanelReference.transform.localScale = Vector3.one; //Do I need to set the local scale?
        summaryPanelReference.gameObject.SetActive(true);
        return summaryPanelReference;
    }

    void Enqueue(GameObject entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        PoolController.Enqueue(p);
    }

    void Clear()
    {
        if(summaryPanelReference != null)
        {
            Enqueue(summaryPanelReference);
            summaryPanelReference = null;
        }
    }


    public void ShowMenu(Unit initiator, Unit target)
    {
        canvas.SetActive(true);
        Clear();

        Dequeue();
        UpdateMenu(initiator, target);
    }

    public void UpdateMenu(Unit initiator, Unit target)
    {
        TargetedActionUnitSummaryPanel panel = summaryPanelReference.GetComponent<TargetedActionUnitSummaryPanel>();
        panel.SetText(panel.playerUnitNameLabel, initiator.name.ToString());
        panel.SetText(panel.playerUnitEquipmentLabel, initiator.equipment.equipmentName);
        panel.SetText(panel.playerHP, initiator.currentHealth.ToString());
        panel.SetText(panel.playerPower, initiator.calculatePower().ToString());
        panel.SetText(panel.playerHit, "75");
        panel.SetText(panel.playerCrit, "0");

        panel.SetText(panel.targetUnitNameLabel, target.name.ToString());
        panel.SetText(panel.targetUnitEquipmentLabel, target.equipment.equipmentName);
        panel.SetText(panel.targetHP, target.currentHealth.ToString());
        panel.SetText(panel.targetPower, target.calculatePower().ToString());
        panel.SetText(panel.targetHit, "75");
        panel.SetText(panel.targetCrit, "0");
    }

    public void HideMenu()
    {
        Clear();
        canvas.SetActive(false);
    }
}
