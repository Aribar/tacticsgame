﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed = 20.0f;
    private Vector3 intendedPosition;
    private Camera battleCamera;

    private Unit trackedUnit;


    private void Awake()
    {
        battleCamera = GetComponentInChildren<Camera>();
        intendedPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if(trackedUnit)
        {
            intendedPosition.x = trackedUnit.transform.position.x;
            intendedPosition.y = trackedUnit.transform.position.y;
        }
        MoveCamera();
    }

    /// <summary>
    /// Move the camera to a new position.
    /// </summary>
    /// <param name="newPosition"></param>
    public void Move(Vector3 newPosition)
    {
        trackedUnit = null;
        intendedPosition = new Vector3(newPosition.x, newPosition.y, transform.position.z);
    }

    /// <summary>
    /// Have the camera track a unit. Lasts until something else moves the camera.
    /// </summary>
    /// <param name="unit"></param>
    public void MoveTrackUnit(Unit unit)
    {
        trackedUnit = unit;
    }

    /// <summary>
    /// This moves the camera with a lerp.
    /// </summary>
    private void MoveCamera()
    {
        Vector3 newPosition = Vector3.Lerp(transform.position, intendedPosition, speed * Time.fixedDeltaTime);
        transform.position = newPosition;
        //yield return StartCoroutine(MoveLerp(newPosition));
    }
}
