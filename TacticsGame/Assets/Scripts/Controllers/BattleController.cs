﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;



public class BattleController : StateMachine
{
    //How many tiles wide and tall should the map be?
    public int mapHeight;
    public int mapWidth;

    //Temporary variables just to make a basic board.
    public TileBase tile1;
    public TileBase tile2;
    public TileBase tileBlank;

    //Temporary variables to make units
    public GameObject heroPrefab;


    //Battle map data
    public BattleBoard board;

    //Current object data
    public Unit currentUnit;
    public Point cursor { get; set; }
    public BattleTile currentTile { get { return board.GetTile(cursor); } }
    public BattleTile lastStartTile;

    public GameObject tileSelector;

    //Action menu stuff???
    public ActionMenuController actionMenu;
    public TargetedActionMenuController targetActionMenu;
    public CameraController cameraController;

    public Canvas ui;

    //Kinda turn based stuff?
    public List<Unit> unitList;
    public int turn;


    // Start is called before the first frame update
    void Start()
    {
        ChangeState<InitBattleState>();
    }

    public bool IsTurnOver()
    {
        foreach(Unit unit in unitList)
        {
            if(unit.CanAct)
                return true;
        }
        turn++;
        Debug.Log("STARTING TURN: " + turn);
        RefreshUnits();
        return false;
    }

    public void RefreshUnits()
    {
        foreach (Unit unit in unitList)
        {
            unit.CanAct = true;
        }
    }

}
