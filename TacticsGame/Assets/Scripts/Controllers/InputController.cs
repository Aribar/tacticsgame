﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputController : MonoBehaviour
{
    Repeater _horizontal = new Repeater("Horizontal");
    Repeater _vertical = new Repeater("Vertical");

    string[] _buttons = new string[] { "Fire1", "Fire2", "Fire3" };

    int addX;
    int addY;

    //public static event EventHandler<InfoEventArgs>

    #region Events
    public static event EventHandler<InfoEventArgs<Point>> moveEvent;
    public static event EventHandler<InfoEventArgs<int>> fireEvent;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int x = _horizontal.Update();
        int y = _vertical.Update();
        if (x != 0 || y != 0)
        {
            moveEvent(this, new InfoEventArgs<Point>(new Point(x, y)));
            //addX += x;
            //addY += y;
            //Debug.Log(addX + ", " + addY);
        }
        //Debug.Log(Input.GetAxisRaw("Horizontal"));
        for (int i = 0; i < 3; ++i)
        {
            if (Input.GetButtonDown(_buttons[i]))
            {
                if (fireEvent != null)
                    fireEvent(this, new InfoEventArgs<int>(i));
            }
        }
    }
}

class Repeater
{
    const float threshold = 0.5f;
    const float rate = .125f;
    float _next;
    bool _hold;
    string _axis;

    public Repeater(string axisName)
    {
        _axis = axisName;
    }

    public int Update()
    {
        int retValue = 0;
        int value = Mathf.RoundToInt(Input.GetAxisRaw(_axis));

        if (value != 0)
        {
            if (Time.time > _next)
            {
                retValue = value;
                _next = Time.time + (_hold ? rate : threshold);
                _hold = true;
            }
        }
        else
        {
            _hold = false;
            _next = 0;
        }

        return retValue;
    }
}