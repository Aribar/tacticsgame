﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectPoolUITest : MonoBehaviour
{
    public List<string> menuEntriesList;
    public Transform menuPanel;
    public GameObject menuCanvas;

    public GameObject menuEntryPrefab;
    public ActionMenuController actionMenu;

    const string EntryPoolKey = "ActionMenu.Entry";

    private void Awake()
    {
        Debug.Log("BEGIN AWAKE");

        //menuEntriesList = new List<string> { "Attack", "Wait" };
        PoolController.AddEntry(EntryPoolKey, menuEntryPrefab, 5, 10);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("BEGIN START");
        AddEntry();

        menuCanvas.SetActive(true);
        //actionMenu.ShowMenu("Menu?", menuEntriesList);

        //actionMenu.Dequeue();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void AddEntry()
    {
        //Debug.Log("Do I make it to Dequeue?");
        ////try
        ////{
        //Poolable p = PoolController.Dequeue(EntryPoolKey);
        ////if (p.GetComponent<MenuEntry>() == null)
        ////{
        ////    Debug.Log("Doesn't have that component.");
        ////    return null;
        ////}
        ////else
        ////{
        ////    Debug.Log("It does have that component.");

        ////}
        //MenuEntry entry = p.GetComponent<MenuEntry>();
        //entry.transform.SetParent(menuPanel, false);
        //Debug.Log("entry.transform.SetParent(panel.transform, false);");
        //entry.transform.localScale = Vector3.one; //Do I need to set the local scale?
        //Debug.Log("entry.transform.localScale = Vector3.one;");
        //entry.gameObject.SetActive(true);
        //Debug.Log("entry.gameObject.SetActive(true);");
        //entry.Reset();
        //Debug.Log("entry.Reset();");
        //return;






        Poolable p = PoolController.Dequeue(EntryPoolKey);
        GameObject entryGO = p.gameObject;
        MenuEntry entry = entryGO.GetComponent<MenuEntry>();


        Debug.Log("Dequeued " + entryGO.ToString());
        entryGO.transform.SetParent(menuPanel, false);
        //entry.transform.SetParent(menuCanvas.transform, false);
        entryGO.gameObject.SetActive(true);

        entry.Title = "test???";

        Debug.Log("Test:" + entry.Title);

        //entry.Reset();

        //Debug.Log("Dequeued " + entry.ToString());
        //entry.transform.SetParent(menuPanel, false);
        ////entry.transform.SetParent(menuCanvas.transform, false);
        //entry.gameObject.SetActive(true);
        entry.Reset();





        //for (int i = 0; i < menuEntriesList.Count; i++)
        //{

        //Item item = itemList[i];
        //GameObject newButton = buttonObjectPool.GetObject();
        //newButton.transform.SetParent(contentPanel);

        //SampleButton sampleButton = newButton.GetComponent<SampleButton>();
        //sampleButton.Setup(item, this);
    }
}