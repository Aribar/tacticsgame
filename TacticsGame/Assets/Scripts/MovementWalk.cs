﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementWalk : Movement
{
    protected override bool ExpandSearch(BattleTile from, BattleTile to)
    {
        if (to.content != null)
            return false;
        return base.ExpandSearch(from, to);
    }

    public override IEnumerator Traverse(BattleTile tile)
    {
        movingFaster = false;
        currentSpeed = baseSpeed;
        unit.Place(tile);

        List<BattleTile> targets = new List<BattleTile>();
        while (tile != null)
        {
            targets.Insert(0, tile);
            tile = tile.previousBattleTile;
        }

        for (int i = 1; i < targets.Count; ++i)
        {
            BattleTile from = targets[i - 1];
            BattleTile to = targets[i];
            yield return StartCoroutine(Walk(to));

        }
        yield return null;
    }

    IEnumerator Walk (BattleTile target)
    {

        LTDescr tweener = transform.LeanMove(target.parentTileMap.GetCellCenterLocal(target.localPosition.ToVector3Int()), currentSpeed);
        while (LeanTween.isTweening(tweener.id))
        {
            yield return null;
        }
           
    }
}
