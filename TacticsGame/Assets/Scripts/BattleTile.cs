﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BattleTile
{
    public Point localPosition { get; set; }
    public Tilemap parentTileMap { get; set; }
    public int movementCost { get; set; }
    //public Unit unit { get; set; }
    public GameObject content;

    //These are for helping with pathfinding
    [HideInInspector] public BattleTile previousBattleTile;
    [HideInInspector] public int distance;

    //public void getTileMapTile (Point p)
    //{

    //}

    //public Vector3 center {  get {  return new Vector3()} }

    public override string ToString()
    {
        
        if (content is null)
            return string.Format("Tile {0} containing nothing.", localPosition.ToString());
        return string.Format("Tile {0} containing {1}.", localPosition.ToString(), content.name);
    }

}
