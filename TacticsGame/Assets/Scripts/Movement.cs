﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    public int range;
    public float baseSpeed = .125f;
    public float currentSpeed;
    public bool movingFaster = false;

    protected Unit unit;

    protected virtual void Awake()
    {
        unit = GetComponent<Unit>();
    }

    public virtual List<BattleTile> GetTilesInRange(BattleBoard board)
    {
        List<BattleTile> retValue = board.Search(unit.tile, ExpandSearch);
        Filter(retValue);
        return retValue;
    }

    protected virtual bool ExpandSearch(BattleTile from, BattleTile to)
    {
        //Debug.Log("Range of this unit: " + range.ToString());
        return (from.distance + 1) <= range;
    }

    /// <summary>
    /// Basic selection filtering. Remove any tiles that contain something besides the unit itself.
    /// </summary>
    /// <param name="tiles"></param>
    protected virtual void Filter(List<BattleTile> tiles)
    {
        for(int i = tiles.Count - 1; i>= 0; --i)
        {
            if(tiles[i].content != null && tiles[i].content != unit.gameObject)
            {
                tiles.RemoveAt(i);
            }
        }
    }

    public abstract IEnumerator Traverse(BattleTile tile);
}
