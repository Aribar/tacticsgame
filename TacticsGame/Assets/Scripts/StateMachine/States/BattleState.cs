﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleState : State
{
    protected BattleController bc;
    //CameraRig?
        //Board?
    public BattleBoard board { get { return bc.board; } }
        //Level data?
    public Dictionary<Point, BattleTile> battleTiles{ get { return bc.board.battleTiles; } }
    public Transform tileSelector { get { return bc.tileSelector.transform; } }
        //Point?
    public Point position { get { return bc.cursor; } set { bc.cursor = value; } }



    protected virtual void Awake()
    {
        bc = GetComponent<BattleController>();
    }

    protected override void AddListeners()
    {
        InputController.moveEvent += OnMove;
        InputController.fireEvent += OnFire;
    }

    protected override void RemoveListeners()
    {
        InputController.moveEvent -= OnMove;
        InputController.fireEvent -= OnFire;
    }

    protected virtual void OnMove (object sender, InfoEventArgs<Point> e)
    {

    }

    protected virtual void OnFire(object sender, InfoEventArgs<int> e)
    {

    }

    protected virtual void SelectTile(Point p)
    {
        if (position == p || !board.battleTiles.ContainsKey(p))
            return;
        position = p;
        tileSelector.localPosition = board.battleTiles[p].parentTileMap.GetCellCenterLocal(battleTiles[p].localPosition.ToVector3Int());
        bc.cameraController.Move(tileSelector.position);
    }

}
