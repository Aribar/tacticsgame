﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseActionState : BattleState
{

    List<string> menuOptions = new List<string> { "Attack", "Wait" };
    public override void Enter()
    {
        base.Enter();
        LoadMenu();
        tileSelector.GetComponent<SpriteRenderer>().enabled = false;
    }

    public override void Exit()
    {
        base.Exit();
        bc.actionMenu.HideMenu();
        tileSelector.GetComponent<SpriteRenderer>().enabled = true;
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        //Debug.Log("MoveTargetState, OnFire: " + sender.ToString() + ", " + e.info.ToString());

        if (e.info.ToString() == "0") //Left Mouse 'A'
        {
            switch(bc.actionMenu.selection)
            {
                case 0: //Attack
                    //Go onto the next thing.
                    Debug.Log("We're going to attack!");
                    bc.ChangeState<ChooseTargetState>();
                    break;
                case 1: //Finish moving
                    Debug.Log("We've moved!");
                    //bc.currentUnit.CanAct = false;
                    //bc.ChangeState<SelectUnitState>();
                    bc.ChangeState<EndUnitTurnState>();
                    break;
            }
        }
        if (e.info.ToString() == "1") //Right Mouse 'B'
        {
            bc.currentUnit.Place(bc.lastStartTile);
            bc.currentUnit.Match();
            SelectTile(bc.lastStartTile.localPosition);
            bc.ChangeState<SelectUnitState>();
        }
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        if (e.info.y < 0)
            bc.actionMenu.ChangeSelection(true);
        else
            bc.actionMenu.ChangeSelection(false);
    }

    public void LoadMenu()
    {
        bc.actionMenu.ShowMenu("Menu!", menuOptions);
    }
}
