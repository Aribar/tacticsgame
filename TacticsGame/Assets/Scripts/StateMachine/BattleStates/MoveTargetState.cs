﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetState : BattleState
{
    List<BattleTile> tiles;

    public override void Enter()
    {
        base.Enter();
        Movement mover = bc.currentUnit.GetComponent<Movement>();
        tiles = mover.GetTilesInRange(board);
        board.SelectTiles(tiles);
    }

    public override void Exit()
    {
        base.Exit();
        board.DeselectTiles(tiles);
        tiles = null;
    }

   protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        SelectTile(e.info + position);
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        if(e.info.ToString() == "0") //Left Mouse 'A'
        {
            if (tiles.Contains(bc.currentTile))
            {
                bc.lastStartTile = bc.currentUnit.tile;
                Debug.Log("lastStartTile set to " + bc.currentUnit.tile.localPosition.ToString());
                bc.ChangeState<MoveSequenceState>();
            }
        }
        else if (e.info.ToString() == "1") //Right Mouse 'B'
        {
            SelectTile(bc.currentUnit.tile.localPosition);
            bc.ChangeState<SelectUnitState>();
        }
    }
}
