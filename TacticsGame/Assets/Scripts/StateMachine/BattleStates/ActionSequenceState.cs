﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSequenceState : BattleState
{
    // Start is called before the first frame update
    public override void Enter()
    {
        base.Enter();

        //Eventually will need something to handle more complicated attacks.
        //owner.currentTile.content.GetComponent<Unit>().currentHealth -= (owner.currentUnit.strength + owner.currentUnit.equipment.might);
        //owner.ChangeState<SelectUnitState>();
        //Debug.Log("test?");

        StartCoroutine("Sequence");
    }

    IEnumerator Sequence()
    {
        yield return null;
        bc.currentTile.content.GetComponent<Unit>().currentHealth -= (bc.currentUnit.strength + bc.currentUnit.equipment.might);
        //bc.currentUnit.CanAct = false;
        //bc.ChangeState<SelectUnitState>();
        bc.ChangeState<EndUnitTurnState>();
    }

}
