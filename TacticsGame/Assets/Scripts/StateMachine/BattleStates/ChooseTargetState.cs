﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseTargetState : BattleState
{
    List<BattleTile> tiles;

    public override void Enter()
    {
        base.Enter();

        //Highlight targets
        tiles = GetTilesInRange(bc.currentUnit);
        board.SelectTiles(tiles);
        bc.targetActionMenu.ShowMenu(bc.currentUnit, bc.currentUnit);
    }

    public override void Exit()
    {
        base.Exit();
        board.DeselectTiles(tiles);
        tiles = null;
        bc.targetActionMenu.HideMenu();
    }

    public List<BattleTile> GetTilesInRange(Unit unit)
    {
        return board.Search(unit.tile, ExpandSearch);
    }

    bool ExpandSearch(BattleTile from, BattleTile to)
    {
        //The 1 on the end will eventually be the attack range...
        return (from.distance + 1) <= 1;
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        SelectTile(e.info + position);
        if (bc.currentTile.content != null)
        {
            if (bc.currentTile.content.GetComponent<Unit>())
            {
                bc.targetActionMenu.UpdateMenu(bc.currentUnit, bc.currentTile.content.GetComponent<Unit>());
            }
            

        }
        //Debug.Log("Targeting tile: " + owner.currentTile.ToString());
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        //Debug.Log("ChooseTargetState, OnFire: " + sender.ToString() + ", " + e.info.ToString());

        if (e.info.ToString() == "0") //Left Mouse 'A'
        {
            if (tiles.Contains(bc.currentTile) && bc.currentTile.content != null)
            {
                Debug.Log("We're targeting this tile!");
                bc.ChangeState<ActionSequenceState>();
            }
        }
        else if (e.info.ToString() == "1") //Right Mouse 'B'
        {
            SelectTile(bc.currentUnit.tile.localPosition);
            bc.ChangeState<ChooseActionState>();
        }
    }
}
