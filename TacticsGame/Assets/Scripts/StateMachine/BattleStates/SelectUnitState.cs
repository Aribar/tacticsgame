﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectUnitState : BattleState
{
    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        SelectTile(e.info + position);
        Debug.Log("On tile: " + bc.currentTile.ToString());
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        //base.OnFire(sender, e);
        if (e.info.ToString() == "0") //Left Mouse 'A'
        {
            GameObject content = bc.currentTile.content;
            if (content.GetComponent<Unit>() != null)
            {
                if(content.GetComponent<Unit>().CanAct)
                {
                    bc.currentUnit = content.GetComponent<Unit>();
                    bc.ChangeState<MoveTargetState>();
                }
            }
        }
        if (e.info.ToString() == "2") //Shift
        {
            GameObject content = bc.currentTile.content;
            if (content != null && content.GetComponent("Unit"))
            {
                Debug.Log("Unit (" + content.name + ") has " + content.GetComponent<Unit>().equipment.equipmentName + " with ability " + content.GetComponent<Unit>().equipment.equipmentActions[0].actionName);
            }
        }
    }
}
