﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InitBattleState : BattleState
{
    // Start is called before the first frame update
    void Start()
    {
        base.Enter();
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        board.InitializeTileMap(bc.mapWidth, bc.mapHeight, bc.tile1, bc.tile2);
        board.InitializeBattleTileData();
        //This will be what initially makes stuff.

        bc.turn = 1;

        SpawnTestUnits();
        yield return null;
        bc.ChangeState<SelectUnitState>();
    }

    void SpawnTestUnits()
    {

        //BattleAction slash = new BattleAction("slash", 1, new List<int> { 1 });
        //BattleAction magicslash = new BattleAction("magicslash", 2, new List<int> { 1 });
        //BattleAction lasershot = new BattleAction("laser", 1, new List<int> { 2 });
        //BattleAction magicblast = new BattleAction("magicblast", 1, new List<int> { 1, 2 });

        Equipment sword = new Equipment("Iron Sword", 6, 70, 5);

        System.Type[] components = new System.Type[] { typeof(MovementWalk), typeof(MovementWalk), typeof(MovementWalk) };
        for(int i = 0; i < 3; i++)
        {
            GameObject instance = Instantiate(bc.heroPrefab) as GameObject;
            instance.name = "Test Unit " + (i + 1).ToString();

            Point p = new Point(i,i);
            Unit unit = instance.GetComponent<Unit>();

            unit.CanAct = true;
            unit.Place(board.GetTile(p));
            unit.equipment = sword;
            unit.maxHealth += i;
            unit.currentHealth = unit.maxHealth;
            unit.strength += i;
            unit.Place(board.GetTile(p));
            unit.Match();

            bc.unitList.Add(unit);
        }



    }
}
