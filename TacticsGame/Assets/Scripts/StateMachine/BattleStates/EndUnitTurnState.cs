﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndUnitTurnState : BattleState
{

    public override void Enter()
    {
        base.Enter();

        StartCoroutine("Sequence");
    }

    IEnumerator Sequence()
    {
        yield return null;
        bc.currentUnit.CanAct = false;
        bc.IsTurnOver();
        bc.ChangeState<SelectUnitState>();
    }
}
