﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSequenceState : BattleState
{
    private Movement m;

    public override void Enter()
    {
        base.Enter();
        StartCoroutine("Sequence");
    }

    IEnumerator Sequence()
    {
        m = bc.currentUnit.GetComponent<Movement>();
        bc.cameraController.MoveTrackUnit(bc.currentUnit);
        yield return StartCoroutine(m.Traverse(bc.currentTile));
        bc.ChangeState<ChooseActionState>();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        if (e.info.ToString() == "0") //Left Mouse 'A'
        {
            if(!m.movingFaster)
            {
                m.currentSpeed *= .75f;
                m.movingFaster = true;
            }
               
        }
    }
}
